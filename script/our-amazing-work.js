const workList = document.querySelector('.work-list');


function filter() {
    const listItems = document.querySelectorAll('.work-list-item');
    workList.addEventListener('click', evt => {
        const targetId = evt.target.dataset.id
        const target = evt.target

        if (target.classList.contains('work-list-item')) {
            listItems.forEach(listItem => listItem.classList.remove('active'))
            target.classList.add('active')
        }

        switch (targetId) {
            case 'all':
                getWorkItems('work-blocks-item')
                break
            case 'graphic':
                getWorkItems(targetId)
                break
            case 'web':
                getWorkItems(targetId)
                break
            case 'landing':
                getWorkItems(targetId)
                break
            case 'wordpress':
                getWorkItems(targetId)
                break
        }
    })
}

filter()

function getWorkItems(className) {
    const workItems = document.querySelectorAll('.work-blocks-item');
    workItems.forEach(item => {
        if (item.classList.contains(className)) {
            item.style.display = 'block'
        } else {
            item.style.display = 'none'
        }
    })
}


function addRow() {
    const workBlocks = document.querySelector('.work-blocks');
    const workBlocksRow = document.querySelector('.work-blocks-row').cloneNode(true);
    const workGraphic = workBlocksRow.querySelector('.graphic');
    const workWeb = workBlocksRow.querySelector('.web');
    const workLanding = workBlocksRow.querySelector('.landing');
    const workWordpress = workBlocksRow.querySelector('.wordpress');

    workGraphic.querySelector('img').src = `./img/graphic%20design/graphic-design${workBlocks.children.length + 1}.jpg`;
    workWeb.querySelector('img').src = `./img/web%20design/web-design${workBlocks.children.length + 1}.jpg`;
    workLanding.querySelector('img').src = `./img/landing%20page/landing-page${workBlocks.children.length + 1}.jpg`;
    workWordpress.querySelector('img').src = `./img/wordpress/wordpress${workBlocks.children.length + 1}.jpg`;

    workBlocks.appendChild(workBlocksRow);
}


for (i = 0; i < 2; i++) addRow()


const buttonWork = document.querySelector('.button-load-work');

buttonWork.addEventListener('click', evt => {
    evt.preventDefault();
    buttonWork.style.display = 'none';
    for (i = 0; i < 3; i++) addRow();
})
